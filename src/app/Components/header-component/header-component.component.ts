import { Component, Input, OnInit, Output } from '@angular/core';
import { SessionService } from 'src/app/Services/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css']
})
export class HeaderComponentComponent implements OnInit {

  @Input() name: string;

  constructor(private session: SessionService, private router: Router) { }

  ngOnInit(): void {
  }

  removeUser(): void {
    this.session.clear();
    this.router.navigateByUrl('/index');
  }
}
