import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../Services/api.service';
import { Location } from '@angular/common';
import { Pokemon } from '../../Models/pokemon';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.css']
})
export class DetailsPageComponent implements OnInit {
  private id: string;
  public isCaught: boolean;
  public pokemon: Pokemon;

  constructor(private route: ActivatedRoute, private api: ApiService, 
    private location: Location, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.api.getPokemonById(this.id).subscribe(updated => {
      this.pokemon = updated;
      this.isCaught = this.api.checkIfCaught(this.id);
    });
  }

  getTypesString(): string {
    let typesString = "Types: ";

    this.pokemon.types.forEach((element, index) => {
      if (index == 0) {
        typesString += element;
      } else {
        typesString += '/' + element;
      }
    });

    return typesString;
  }

  backToCatalogueClick(): void {
    this.location.back();
  }

  catchPokemonClick(): void {
    this.api.catchPokemon(this.id);
    this.router.navigateByUrl('/catalogue/true');
  }

  releasePokemonClick(): void {
    this.api.releasePokemon(this.id);
    this.router.navigateByUrl('/catalogue/true');
  }
}
