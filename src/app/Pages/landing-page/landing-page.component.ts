import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from '../../Services/session.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [Validators.required,
                                      Validators.minLength(2)])
  });

  constructor(private router: Router, private session: SessionService) { }

  
  get trainerName() {
    return this.loginForm.get('trainerName'); 
  }
  

  ngOnInit(): void {
    if (this.session.getTrainer) {
      this.router.navigateByUrl(`/catalogue/${ Boolean(true) }`);
    }
  }

  onNameEntered(): void {
    //Navigate to trainer page when name entered
    this.session.createTrainer(this.trainerName.value);
    this.router.navigateByUrl(`/catalogue/${ Boolean(true) }`);
  }

}
