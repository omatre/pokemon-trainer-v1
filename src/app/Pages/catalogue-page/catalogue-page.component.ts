import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Pokemon } from '../../Models/pokemon';
import { ApiService } from '../../Services/api.service';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue-page.component.html',
  styleUrls: ['./catalogue-page.component.css']
})
export class CataloguePageComponent implements OnInit {
  private trainerBtnElement: HTMLElement;
  private catalogueBtnElement: HTMLElement;
  private morePokemonsBtnElement: HTMLElement;
  public pokemons: Pokemon[] = [];
  public types: string[] = [];
  private filteredTypes: string[] = [];
  private countPokemonIntervals: number = 1;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.trainerBtnElement = document.getElementById('trainer-button');
    this.catalogueBtnElement = document.getElementById('catalogue-button');
    this.morePokemonsBtnElement = document.getElementById('more-pokemons');

    //Render all types from api call
    this.api.getAllTypes();
    this.api.types.subscribe(updated => {
      this.types = updated;
    })

    //Render trainer page or catalogue page
    if (this.route.snapshot.paramMap.get('isCaught') == "true") {
      this.trainerBtnElement.click();
    } else {
      this.catalogueBtnElement.click();
    }
  }

  setBackgroundColor(type): string {
    switch (type) {
      case 'fire':
        return '#EE8130';
      case 'water':
        return '#6390F0';
      case 'grass':
        return '#7AC74C';
      case 'electric':
        return '#F7D02C';
      case 'poison':
        return '#A33EA1';
      case 'fairy':
        return '#D685AD';
      case 'bug':
        return '#A6B91A';
      case 'normal':
        return '#A8A77A';
      case 'flying':
        return '#A98FF3';
      case 'ground':
        return '#E2BF65';
      case 'fighting':
        return '#C22E28';
      case 'ice':
        return '#96D9D6';
      case 'dragon':
        return '#6F35FC';
      case 'ghost':
        return '#735797';
      case 'dark':
        return '#705746';
      case 'rock':
        return '#B6A136';
      case 'psychic':
        return '#F95587';
      case 'steel':
        return '#B7B7CE';
      case 'shadow':
        return '#111077';
      case 'unknown':
        return '#212138'
    
      default:
        return 'background-color: grey;';
    }
  }

  pokemonClicked(id: number | string): void {
    this.router.navigateByUrl(`details/${ id }`);
  }

  filterByTypeButtonClick(type: string): void {
    //Toggle button
    let btnElement = document.getElementById(`${ type }`);

    //Toggle type to/from list
    if (this.filteredTypes.some(element => element == type)) {
      this.filteredTypes = this.filteredTypes.filter(element => element != type);
      btnElement.style.opacity = "1.0";

    } else {
      this.filteredTypes.push(type);
      btnElement.style.opacity = "0.2";
    }
    
    //Render trainer page or catalogue page
    if (this.trainerBtnElement.style.display == "none") {
      this.onTrainerButtonClick();
    } else {
      this.onCatalogueButtonClick();
    }
  }

  onMorePokemonsButtonClick(): void {
    //Render 20 more pokemons
    this.countPokemonIntervals++;
    this.api.getAllPokemon(this.countPokemonIntervals);
  }

  onTrainerButtonClick(): void {
    //Render button for general catalogue and display all pokemon
    this.router.navigateByUrl('/catalogue/true');
    this.catalogueBtnElement.style.display = "block";
    this.trainerBtnElement.style.display = "none";
    this.morePokemonsBtnElement.style.display = "none";
    this.api.clearPokemonList();
    this.api.updateCaughtIds();
    this.api.getCaughtPokemon();
    this.api.pokemons.subscribe(updated => {
      this.pokemons = updated;
      this.pokemons.sort((a,b) => a.id - b.id);

      //Filter pokemons by filtered types
      let pokemonsTemp = [];
      for (const pokemon of this.pokemons) {
        let isFilteredOut = false;
        for (const type of this.filteredTypes) {
          if (pokemon.types.some(element => element == type)) {
            isFilteredOut = true;
          }
        }
        if (!isFilteredOut) {
          pokemonsTemp.push(pokemon);
        }
      }
      this.pokemons = pokemonsTemp;
    });
  }

  onCatalogueButtonClick(): void {
    //Render button for trainer catalogue and display only caught pokemon
    this.router.navigateByUrl('/catalogue/false');
    this.trainerBtnElement.style.display = "block";
    this.catalogueBtnElement.style.display = "none";
    this.morePokemonsBtnElement.style.display = "block";
    this.api.clearPokemonList();
    this.countPokemonIntervals = 1;
    this.api.getAllPokemon(this.countPokemonIntervals);
    this.api.pokemons.subscribe(updated => {
      this.pokemons = updated;
      this.pokemons.sort((a,b) => a.id - b.id);
      
      //Filter pokemons by filtered types
      let pokemonsTemp = [];
      for (const pokemon of this.pokemons) {
        let isFilteredOut = false;
        for (const type of this.filteredTypes) {
          if (pokemon.types.some(element => element == type)) {
            isFilteredOut = true;
          }
        }
        if (!isFilteredOut) {
          pokemonsTemp.push(pokemon);
        }
      }
      this.pokemons = pokemonsTemp;
    })
  }
}
