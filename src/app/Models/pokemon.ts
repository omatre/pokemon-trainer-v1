import { Stat } from './stat';

export class Pokemon {
    public id: number;
    public name: string;
    public image: string;
    public types: string[];
    public stats: Stat;

    constructor(id: string, name: string, image: string, types: string[], stats: Stat){
        this.id = parseInt(id);
        this.name = name;
        this.image = image;
        this.types = types;
        this.stats = stats;
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}
