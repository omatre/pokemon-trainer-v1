export class Stat {
    public hp: string;
    public weight: string;
    public speed: string;
    public attack: string;
    public defence: string;
    public specialAttack: string;
    public specialDefence: string;

    constructor(hp: string, attack: string, 
        defence: string, specialAttack: string,
        specialDefence: string, speed: string,
        weight: string) {
            this.hp = hp;
            this.attack = attack;
            this.defence = defence;
            this.specialAttack = specialAttack;
            this.specialDefence = specialDefence;
            this.speed = speed;
            this.weight = weight;
    }
}
