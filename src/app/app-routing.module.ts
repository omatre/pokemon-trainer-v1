import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guard/auth/auth.guard';
import { CataloguePageComponent } from './Pages/catalogue-page/catalogue-page.component';
import { DetailsPageComponent } from './Pages/details-page/details-page.component';
import { LandingPageComponent } from './Pages/landing-page/landing-page.component';

const routes: Routes = [
  {
    path: 'index',
    component: LandingPageComponent
  },
  {
    path: 'catalogue/:isCaught',
    component: CataloguePageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'details/:id',
    component: DetailsPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'index',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
