import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pokemon } from '../Models/pokemon';
import { Stat } from '../Models/stat';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private caughtId: string[] = [];
  readonly totalNumberPokemon: number = 1050;
  private _pokemons = new BehaviorSubject<Pokemon[]>([]);
  private _types = new BehaviorSubject<string[]>([]);
  private datastore: { pokemons: Pokemon[], types: string[] } = { pokemons: [], types: [] };
  readonly pokemons = this._pokemons.asObservable();
  readonly types = this._types.asObservable();

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.updateCaughtIds();
  }

  public checkIfCaught(id: string): boolean {
    return this.caughtId.some(element => element == id);
  }

  public catchPokemon(id: string): void {
    if (localStorage.getItem('caughtIds') == null) {
      localStorage.setItem('caughtIds', `${ id }`);
    } else {
      localStorage.setItem('caughtIds', `${ localStorage.getItem('caughtIds') }, ${ id }`);
    }
    this.updateCaughtIds();
  }

  public releasePokemon(id: string): void {
    let index = this.caughtId.findIndex(el => el == id);
    this.caughtId.splice(index, 1);
    let caughtIdToLocalStorage = "";
    this.caughtId.forEach((element, index) => {
      if (index < this.caughtId.length - 1) {
        caughtIdToLocalStorage += element.toString() + ', ';
      } else {
        caughtIdToLocalStorage += element.toString();
      }
    });
    localStorage.setItem('caughtIds', caughtIdToLocalStorage);
    console.log(caughtIdToLocalStorage + ' = ' + localStorage.getItem('caughtIds'));
  }

  public updateCaughtIds(): void {
    let caughtFromLocalStorage = localStorage.getItem('caughtIds');
    if (caughtFromLocalStorage) {
      this.caughtId = caughtFromLocalStorage.split(', ');
    } else {
      this.caughtId = [];
    }
  }

  public clearPokemonList(): void {
    //Clear list to avoid duplicate when page renders again
    while (this.datastore.pokemons.length > 0) {
      this.datastore.pokemons.pop();
    }
  }

  public getCaughtPokemon(): void {
    //Save each api-response with id from caughtId in an array of objects
    if (this.caughtId.length > 0) {
      this.caughtId.forEach(id => {
        this.getPokemonById(id).subscribe(data => {
          this.datastore.pokemons.push(data);
          this._pokemons.next(Object.assign({}, this.datastore).pokemons);
        });
      });
    } else {
      this.datastore.pokemons = [];
    }
  }

  public getAllPokemon(nth: number): void {
    let maximum = nth * 20;
    if (maximum > this.totalNumberPokemon) {
      maximum = this.totalNumberPokemon;
    } else if ((nth - 1) * 20 > this.totalNumberPokemon) {
      return;
    }
    for (let i = ((nth - 1) * 20) + 1; i <= maximum; i++) {
      if (i <= 893) {
        this.getPokemonById(i).subscribe(data => {
          this.datastore.pokemons.push(data);
          this._pokemons.next(Object.assign({}, this.datastore).pokemons);
        })
      } else {
        this.getPokemonById(i + 9107).subscribe(data => {
          this.datastore.pokemons.push(data);
          this._pokemons.next(Object.assign({}, this.datastore).pokemons);
        })
      }
    }
  }

  public getAllTypes(): Subscription {
    return this.getAllTypesCall().subscribe(res => {
      this.datastore.types = res;
      this._types.next(Object.assign({}, this.datastore).types);
    });
  }

  public getPokemonById(id: number | string) : Observable<Pokemon> {
    return this.http.get<any>(`https://pokeapi.co/api/v2/pokemon/${ id }`).pipe(
      map(data => new Pokemon(data.id, 
                              data.name, 
                              data.sprites.front_default, 
                              data.types.map(element => element.type.name),
                              new Stat(data.stats[0].base_stat,
                                      data.stats[1].base_stat,
                                      data.stats[2].base_stat,
                                      data.stats[3].base_stat,
                                      data.stats[4].base_stat,
                                      data.stats[5].base_stat,
                                      data.weight)
                              )
    ));
  }

  private getAllTypesCall() : Observable<string[]> {
    return this.http.get<any>('https://pokeapi.co/api/v2/type/').pipe(
      map(data => {
        return data.results.map(element => {
          return element.name;
        }
      )})
    );
  }
}
