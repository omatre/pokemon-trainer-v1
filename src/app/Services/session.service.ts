import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  
  private trainerName: string = '';

  constructor() { }

  public createTrainer(name: string): void {
    this.trainerName = name;
    localStorage.setItem('name', name);
  }

  public get getTrainer() : string {
    this.trainerName = localStorage.getItem('name');
    return this.trainerName;
  }

  public clear(): void {
    this.trainerName = '';
    localStorage.clear();
  }
  
}
