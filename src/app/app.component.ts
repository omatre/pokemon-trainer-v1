import { Component } from '@angular/core';
import { SessionService } from './Services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pokemon-trainer-v1';

  constructor(public session: SessionService) {}
}
