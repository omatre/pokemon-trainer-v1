# PokemonTrainerV1

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.2.

## Description

Pokemon Trainer is a game where you can find all existing pokemon and filter them by types. The pokemon can be catched and released. The game can be viewed in https://pokemon-trainer-3000.herokuapp.com/.

## TODO

* [x] Create pages
* [x] Create components
* [x] Add routing
* [x] Add navigation logic
* [x] Add mock data
* [x] Design pages
* [x] Create API methods
* [x] Caught pokemon logic
* [x] Add AuthGuard
* [x] Implement models
* [x] Add favicon and title
* [x] Add type and color to filter-buttons and top of cards

## Bugs/Improvements

* [ ] Slow api-calls when calling high numbers
* [ ] Clearing pokemons higher than 20 when hitting filter buttons
* [x] console.log: ctx.types is undefined
* [x] console.log: caughtIds start with null
* [x] console.log: ctx.pokemon.name is undefined, when going to details
* [x] Not images for all pokemons (replace with shadow image?)
* [x] Always going back to either catalogue or trainer when returning from details
* [x] Filter not working

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
